
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.Scanner;
import utfpr.ct.dainf.if62c.pratica.ContadorPalavras;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * IF62C - Fundamentos de Programação 2
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica72 {
    public static void main(String[] args) throws FileNotFoundException, IOException{
        Scanner leitor = new Scanner(System.in);
        String nome;
        System.out.println("Qual é o nome do arquivo de texto?");
        ContadorPalavras haruno;
        nome=leitor.next();
        File arquivo= new File(nome);
        haruno= new ContadorPalavras(arquivo);
        Map<String,Integer> sakura;
        sakura=haruno.getPalavras();
        try (BufferedWriter escritor = new BufferedWriter(new FileWriter(nome+".out")); BufferedReader leite = new BufferedReader(new FileReader(arquivo))) {
            String linha;
            Collection cole=sakura.entrySet();
            while((linha = leite.readLine())!=null){
                escritor.write(linha);
                escritor.newLine();
            }
        }
    }
}
