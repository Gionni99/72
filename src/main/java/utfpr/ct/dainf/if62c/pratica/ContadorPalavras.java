/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Giovanni Bandeira
 */
public class ContadorPalavras {

    private BufferedReader reader;
    
    public ContadorPalavras(File arquivo) throws FileNotFoundException {
        this.reader = new BufferedReader(new FileReader(arquivo));
    }
    
    public Map getPalavras() throws IOException{
        String linha;
        Map<String,Integer> mapito;
        mapito = new HashMap<>();
        while((linha = reader.readLine())!= null)
        {
            if(mapito.containsKey(linha))
                mapito.put(linha, mapito.get(linha)+1);
            else
                mapito.put(linha, 1);
        }
        reader.close();
        return mapito;
    }
}
